function checkboxes(){
    var inputElems = document.getElementsByTagName("input"),
    count = 0;
    var images = "";
    for (var i=0; i<inputElems.length; i++) {
        if (inputElems[i].type == "checkbox" && inputElems[i].checked == true) {
            count++;
            if(count>1)
                images+=";";
            images += inputElems[i].id;
        }
        document.getElementById("selectedBadge").textContent=count;
    }
    document.getElementById("checkedImages").value = images;
    console.log(images);
}

function operationBtnAction() {
    if(document.getElementById("selectedBadge").textContent!="0")
        document.getElementById('form2').submit();
    else
        alert("You must select at least one photo!");
}

$(document).ready(function(){
    checkboxes();
});

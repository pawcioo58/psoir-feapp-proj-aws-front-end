package com.kenaz.psoir.feapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FeappApplication {

	public static void main(String[] args) {
		AwsStart.init();
		indexController.Log(LogType.INFO,"Initializing application...");
		SpringApplication.run(FeappApplication.class, args);
		indexController.Log(LogType.INFO,"Application running");
	}
}

package com.kenaz.psoir.feapp;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.simpledb.AmazonSimpleDB;
import com.amazonaws.services.simpledb.AmazonSimpleDBClient;
import com.amazonaws.services.simpledb.model.CreateDomainRequest;
import com.amazonaws.services.simpledb.model.PutAttributesRequest;
import com.amazonaws.services.simpledb.model.ReplaceableAttribute;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import org.joda.time.DateTime;

import java.util.List;

public class AwsStart {
    public static String imageUrlPrefix = "https://s3-us-west-2.amazonaws.com/";
    private static AmazonS3 s3client = null;
    private static List<Bucket> buckets=null;
    private static AmazonSQS sqs = null;
    private static AmazonDynamoDB  dynamoDBc = null;
    private static DynamoDB dynamoDB = null;

    public static void init(){

        AWSCredentials credentials = new BasicAWSCredentials(Config.getAwsAccessKeyId(),Config.getAwsSecretKey());
        s3client = AmazonS3ClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(Regions.US_WEST_2)
                .build();
        sqs = AmazonSQSClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(Regions.US_WEST_2)
                .build();
        dynamoDBc = AmazonDynamoDBClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(Regions.US_WEST_2)
                .build();
        dynamoDB = new DynamoDB(dynamoDBc);

        buckets = s3client.listBuckets();
    }

    public static AmazonS3 getS3Client() {
        return s3client;
    }

    public static AmazonSQS getSqsClient() {
        return sqs;
    }

    public static DynamoDB getDynamoDB() {
        return dynamoDB;
    }

    public static String getImageBucket() throws Exception {
        for (Bucket bucket: buckets) {
            if(bucket.getName().matches(Config.getBucket())){
                return bucket.getName();
            }
        }
            throw new Exception("Image bucket '"+Config.getBucket()+"' not exist on S3 server");
    }
}

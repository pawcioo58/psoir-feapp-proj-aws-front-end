package com.kenaz.psoir.feapp.Models;

public class Image {
    public String src;
    public String fileName;


    public Image(String src, String fileName) {
        this.src = src;
        this.fileName = fileName;
    }
}

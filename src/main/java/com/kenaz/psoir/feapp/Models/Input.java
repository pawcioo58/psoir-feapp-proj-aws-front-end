package com.kenaz.psoir.feapp.Models;

public class Input {
   public String type;
   public String name;
   public String value;

    public Input(String type, String name, String value) {
        this.type = type;
        this.name = name;
        this. value = value;
    }
}

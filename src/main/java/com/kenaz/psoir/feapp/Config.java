package com.kenaz.psoir.feapp;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class Config {
    private static final String ConfigFileName = "config.txt";
    private static Map<String,String> configData;

    public static String getServerUrl(){
        updateConfigData();
        return configData.get("server_url");
    }

    private static void updateDataFromFile() throws IOException {
        configData =  new HashMap<>();
        for (String line : Files.readAllLines(Paths.get(ConfigFileName))) {
            if(line.length()>4) {
                if (!line.substring(0, 3).contains("#")) {
                    if (line.contains("=")) {
                        String[] data = line.split("=");
                        configData.put(data[0], data[1]);
                    }
                }
            }
        }
    }

    public static int getExpirationTime() {
        int timeInMinutes=60;
        updateConfigData();
       try{
           int time = Integer.valueOf(configData.get("expiration_time"));
           timeInMinutes = time;
       }catch (Exception ex){}
        return timeInMinutes;
    }

    public static String getBucket() {
        updateConfigData();
        return configData.get("bucket_name");
    }

    private static void loadDefault() {
        System.out.println("Problem occurres during load configuration from file. Loading defaults configuration...");
        configData =  new HashMap<>();
        configData.put("server_url","http://54.213.208.160:8080/");
        configData.put("expiration_time", "60");
        configData.put("bucket_name","photo-files-psoir-bucket");
        configData.put("aws_access_key_id","AKIAIFQQDKRKMEN4RJ3A");
        configData.put("aws_secret_key","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");
        configData.put("sqs_name","operationQueue.fifo");
    }

    public static String getAwsAccessKeyId() {
        updateConfigData();
        return configData.get("aws_access_key_id");
    }

    public static String getAwsSecretKey() {
        updateConfigData();
        return configData.get("aws_secret_key");
    }

    public static String getImagePath() {
        updateConfigData();
        return configData.get("image_path");
    }

    public static String getSqsName(){
        updateConfigData();
        return configData.get("sqs_name");
    }

    private static void updateConfigData() {
        try {
            updateDataFromFile();
        } catch (IOException e) {
            e.printStackTrace();
            loadDefault();
        }
    }
}

package com.kenaz.psoir.feapp;

public enum LogType {
    INFO("INFO"),
    WARNING("WARNING"),
    ERROR("ERROR");

    private final String type;

    private LogType(final String type) {
        this.type = type;
    }
    @Override
    public String toString() {
        return type;
    }
}

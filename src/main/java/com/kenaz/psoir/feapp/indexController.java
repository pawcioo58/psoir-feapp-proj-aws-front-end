package com.kenaz.psoir.feapp;

import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.*;
import com.kenaz.psoir.feapp.Models.Image;
import com.kenaz.psoir.feapp.Models.Input;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
public class indexController {

    @RequestMapping("/")
    String index(@RequestParam(value = "delete", required = false) String deleteFileName, Model model) throws Exception {
        Log(LogType.INFO,"Main page content request");
        if (deleteFileName != null) {
            Log(LogType.INFO,"Deleting file: " + deleteFileName);
            System.out.println("Deleting file: " + deleteFileName);
            AmazonS3 s3client = AwsStart.getS3Client();
            s3client.deleteObject(new DeleteObjectRequest(Config.getBucket(), Config.getImagePath() + deleteFileName));
            Log(LogType.INFO,"File: " +deleteFileName + " removed successfully");
        }
        String policyFile = null;
        try {
            policyFile = Helpers.readFile("policy.json", StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }

        policyFile = policyFile.replace("expiration-date", Helpers.getExpirationDate(Config.getExpirationTime()));
        policyFile = policyFile.replace("/success_upload", Config.getServerUrl() + "/success_upload");
        policyFile = policyFile.replace("images/", Config.getImagePath());
        System.out.println("Police content:" + policyFile);


        byte[] encodedBytes = Base64.getEncoder().encode(policyFile.getBytes("UTF-8"));
        String policyBytesString = new String(encodedBytes, "UTF-8");

        String signature = Helpers.hmacSha1(policyBytesString, Config.getAwsSecretKey());

        System.out.println("Policy Base64 :" + new String(encodedBytes, "UTF-8"));
        System.out.println("Signature     :" + signature);

        AmazonS3 s3client = AwsStart.getS3Client();


        List<Input> inputs = new ArrayList<>();
        inputs.add(new Input("hidden", "key", Config.getImagePath() + "${filename}"));
        inputs.add(new Input("hidden", "acl", "public-read"));
        inputs.add(new Input("hidden", "success_action_redirect", Config.getServerUrl() + "/success_upload"));
        inputs.add(new Input("hidden", "AWSAccessKeyId", Config.getAwsAccessKeyId()));
        inputs.add(new Input("hidden", "policy", policyBytesString));
        inputs.add(new Input("hidden", "signature", signature));
        inputs.add(new Input("hidden", "Cache-Control", "max-age=0"));

        List<Image> images = new ArrayList<>();
        ObjectListing objectListing = s3client.listObjects(AwsStart.getImageBucket());
        for (S3ObjectSummary os : objectListing.getObjectSummaries()) {
            if (os.getKey().substring(os.getKey().indexOf('/')).length() > 2)
                if (os.getKey().contains(Config.getImagePath()))
                    images.add(new Image(AwsStart.imageUrlPrefix + AwsStart.getImageBucket() + "/" + os.getKey(), os.getKey().substring(os.getKey().indexOf('/') + 1)));
        }

        model.addAttribute("form_inputs", inputs);
        model.addAttribute("images", images);
        model.addAttribute("server_url", Config.getServerUrl());
        return "index";
    }

    @RequestMapping("/success_upload")
    String successUpload(@RequestParam(value = "bucket", required = false) String bucket,
                         @RequestParam(value = "key", required = false) String key,
                         @RequestParam(value = "etag", required = false) String etag,
                         Model model) {
        Log(LogType.INFO,"File uploaded succesfully bucket:" +bucket + " key:"+key + " etag:"+etag);
        model.addAttribute("bucket", bucket);
        if (key != null)
            model.addAttribute("file_name", key.substring(key.indexOf('/') + 1));
        return "success_upload";
    }

    @RequestMapping("/perform_action")
    String performAction(@RequestParam(value = "operation") String operation,
                         @RequestParam(value = "checkedImages", required = false) String checkedImages,
                         Model model) {
        String[] imagesNames = checkedImages.split(";");
        Log(LogType.INFO,"New Action:" +operation + " on "+imagesNames.length + " element(s)");
        AmazonSQS sqs = AwsStart.getSqsClient();
        ListQueuesResult lq_result = sqs.listQueues();
        System.out.println("Your SQS Queue URLs:");
        String sqs_url = "";
        for (String url : lq_result.getQueueUrls()) {
            if (url.contains(Config.getSqsName())) {
                System.out.println(url);
                sqs_url = url;
            }
        }
        /*for(String image : imagesNames){
            System.out.println("Adding to queue: " + image);
            MessageAttributeValue operationAttr = new MessageAttributeValue();
            operationAttr.setDataType("String");
            operationAttr.setStringValue(operation);

            MessageAttributeValue keyAttr = new MessageAttributeValue();
            keyAttr.setDataType("String");
            keyAttr.setStringValue(Config.getImagePath()+image);

            SendMessageRequest send_msg_request = new SendMessageRequest()
                    .withQueueUrl(sqs_url)
                    .withMessageGroupId("group1")
                    .withMessageDeduplicationId("DID_"+String.valueOf(System.currentTimeMillis()))
                    .withMessageBody("Operation: " + operation + "\nFile name: " + image)
                    .addMessageAttributesEntry("operation", operationAttr)
                    .addMessageAttributesEntry("key", keyAttr);
            sqs.sendMessage(send_msg_request);
        }*/
        System.out.println("Sending data to sqs");
        Log(LogType.INFO,"Sending data to SQS");
        int i = 0;
        List<SendMessageBatchRequestEntry> entries = new ArrayList<>();
        for (String image : imagesNames) {
            System.out.println("Adding to queue: " + image);
            MessageAttributeValue operationAttr = new MessageAttributeValue();
            operationAttr.setDataType("String");
            operationAttr.setStringValue(operation);

            MessageAttributeValue keyAttr = new MessageAttributeValue();
            keyAttr.setDataType("String");
            keyAttr.setStringValue(Config.getImagePath() + image);
            SendMessageBatchRequestEntry entry = new SendMessageBatchRequestEntry();
            entry.setId("_EID_" + String.valueOf(System.currentTimeMillis()) + i);
            entry.setMessageGroupId("group1");
            entry.setMessageDeduplicationId("DID_" + (String.valueOf(System.currentTimeMillis()) + i));
            entry.setMessageBody("Operation: " + operation + "\nFile name: " + image);
            entry.addMessageAttributesEntry("operation", operationAttr);
            entry.addMessageAttributesEntry("key", keyAttr);
            entries.add(entry);
            if (entries.size() == 10) {
                SendMessageBatchRequest send_batch_request = new SendMessageBatchRequest()
                        .withQueueUrl(sqs_url)
                        .withEntries(entries);

                sqs.sendMessageBatch(send_batch_request);

                entries.clear();
            }
            i++;
        }
        if (entries.size() > 0) {
            SendMessageBatchRequest send_batch_request = new SendMessageBatchRequest()
                    .withQueueUrl(sqs_url)
                    .withEntries(entries);
            sqs.sendMessageBatch(send_batch_request);
        }
        Log(LogType.INFO,"Sending data to SQS: DONE");
        System.out.println("Done");

        if (imagesNames != null) {
            model.addAttribute("listSize", imagesNames.length);
        }
        model.addAttribute("bucket_name", Config.getBucket());
        model.addAttribute("operation", operation);
        model.addAttribute("sqs_url", sqs_url);
        model.addAttribute("mainpage_url", Config.getServerUrl());
        return "perform_action";
    }

    public static void Log(LogType type, String message){
        DynamoDB dynamoDB = AwsStart.getDynamoDB();
        QuerySpec spec = new QuerySpec();
        Table table = dynamoDB.getTable("LogsFeApp");
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss.SSS dd-MM-yyy");
        String date = dateFormat.format(new Date());
        ItemCollection<QueryOutcome> resultsAll = table.query(spec);
        Item item = new Item().with("Type", type.toString())
                .withString("Date", date)
                .withString("Message", message);
        table.putItem(item);
    }
}